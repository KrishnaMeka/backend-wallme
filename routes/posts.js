import express from 'express';
import { getPosts, createPost, deletePost, likePost, comment, replyPost } from '../controllers/posts.js'

const router = express.Router();

router.get('/', getPosts);
router.post('/', createPost);
router.delete('/:id', deletePost)
router.put('/:id/:name', likePost)
router.put('/comment', comment)
router.put('/reply', replyPost)

export default router;