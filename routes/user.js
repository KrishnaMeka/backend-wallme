import express from 'express';
import { registerUser, loginUser, updateUser } from '../controllers/users.js';

const router = express.Router();

router.post('/register', registerUser);
router.post('/login', loginUser);
router.put('/updateUser', updateUser)

export default router;
