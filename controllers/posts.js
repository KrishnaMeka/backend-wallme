import PostMessage from '../models/postMessage.js';
import mongoose from 'mongoose';

export const getPosts = async (req, res) => {
    try {
        const postMessages = await PostMessage.find().sort({ _id: -1 });;
        res.status(200).send(postMessages)
    } catch (error) {
        res.status(404).send({ message: error.message })
    }
}

export const createPost = async (req, res) => {
    const post = req.body
    const newPost = new PostMessage(post);

    try {
        const result = await newPost.save();
        res.status(201).send(result);
    } catch (error) {
        res.status(409).send({ message: error.message });
    }
}

export const deletePost = async (req, res) => {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send({ message: `No post with id: ${id}` });
    try {
        await PostMessage.deleteOne({ _id: id });
        res.status(200).send({ message: 'deleted!' });
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
}

export const likePost = async (req, res) => {
    const { id, name: lastName } = req.params;
    try {
        let post = await PostMessage.findById({ _id: id });
        if (post) {
            if (post.likeCount.indexOf(lastName) > -1) {
                post.likeCount = post.likeCount.filter((like) => like !== lastName);
            } else post.likeCount.push(lastName);
            const result = await post.save();
            res.status(200).send(result);
        } else {
            res.status(404).send({ message: "Post did not find" });
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
}

export const comment = async (req, res) => {
    const { comment, user, postId } = req.body;
    try {
        let post = await PostMessage.findById({ _id: postId });
        if (post) {
            let obj = {}
            obj.comment = comment;
            obj.user = user;
            obj.replies = []
            post.comments.push(obj);
            const result = await post.save();
            res.status(200).send(result);
        } else {
            res.status(404).send({ message: "Post did not find" });
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
}

export const replyPost = async (req, res) => {
    const { reply, user, commentId } = req.body;
    try {
        let post = await PostMessage.findOne({ "comments._id": commentId });
        if (post) {
            let obj = {}
            obj.reply = reply;
            obj.user = user;
            let targetComment = post.comments.find(comment => comment._id.toString() === commentId);
            targetComment.replies.push(obj);
            const result = await post.save();
            res.status(200).send(result);
        } else {
            res.status(404).send({ message: "Post did not find" });
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
}