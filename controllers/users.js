import User from '../models/user.js';

export const registerUser = async (req, res) => {
    const user = req.body;
    try {
        let userExists = await User.findOne({ email: user.email });
        if (userExists) return res.status(400).send({ message: "User already registered" });

        const newUser = new User(user);
        await newUser.save();
        res.status(200).send({ message: 'User registered Successfully' });
    } catch (error) {
        res.status(400).send(error);
    }
}

export const loginUser = async (req, res) => {
    try {
        let user = await User.findOne({ email: req.body.email });
        if (!user) return res.status(400).send({ message: "Invalid Email or Password" });
        if (user.password !== req.body.password) return res.status(400).send({ message: "Invalid Email or Password" });
        const resObj = {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            isAdmin: user.isAdmin,
            profileDp: user.profileDp,
            id: user._id
        }
        res.status(200).send(resObj);
    } catch (error) {
        res.status(400).send(error)
    }
}

export const updateUser = async (req, res) => {
    const { firstName, lastName, email, password, id } = req.body;
    try {
        let user = await User.findById({ _id: id });
        if (user) {
            user.firstName = firstName;
            user.lastName = lastName;
            user.email = email;
            if (password !== '') {
                console.log("---------passUser-----", firstName, lastName, email, password, id);
                user.password = password;
            }
            const result = await user.save();
            res.status(200).send(result);
        } else res.status(404).send({ message: "User not found" });
    } catch (error) {
        res.status(404).send({ message: error.message });
    }
}
