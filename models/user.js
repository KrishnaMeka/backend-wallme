import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  firstName: {
    type: String,
    minlength: 3,
    maxlength: 50,
    required: true,
  },
  lastName: {
    type: String,
    minlength: 3,
    maxlength: 50,
    required: true,
  },
  email: {
    type: String,
    minlength: 10,
    maxlength: 150,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    minlength: 5,
    maxlength: 50,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  profileDp: String,
});

const User = mongoose.model("User", userSchema);

export default User;
