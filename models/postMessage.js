import mongoose from "mongoose";

const postSchema = mongoose.Schema({
  message: String,
  creator: String,
  selectedFile: String,
  likeCount: [{ type: String }],
  comments: [{
    user: String,
    comment: String,
    commentAt: { type: Date, default: () => { return new Date() } },
    replies: [{ user: String, reply: String, repliedAt: { type: Date, default: () => { return new Date() } } }]
  }],
  createdAt: {
    type: Date,
    default: () => { return new Date() }
  },
});

const PostMessage = mongoose.model("PostMessage", postSchema);

export default PostMessage;
